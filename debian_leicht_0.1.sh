##  Debian Leicht - Script para la fácil configuración de Debian
##  Copyright 2015  Víctor Salmerón <viktormx@protonmail.ch>
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
if [[ $EUID -ne 0 ]]; then
	clear
	echo "este script debe ser ejecutado por el usuario root"  1>&2
	exit 1
else
	while :
	 do
	 clear;
	 echo " Escoja una opción "
	 echo "1. Configurar repositorios"
	 echo "2. Actualizar el sistema operativo"
	 echo "3. Instalar un entorno de escritorio"
	 echo "4. Salir"
	 echo -n "Seleccione una opcion [1 - 4]"
	 read opcion
	 case $opcion in
	 1) sh data/repos.sh;;

	 2) sh data/actualizar.sh;;

	 3) sh data/escritorio.sh;;

	 4) echo "adiós";
	 		clear;
	 		exit 1;;
	 *) echo "$opc es una opción invalida.";
	 echo "Presiona una tecla para continuar...";
	 read foo;;
 	esac
	done
fi
