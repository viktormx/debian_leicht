##  Debian Leicht - Script para la fácil configuración de Debian
##  Copyright 2015  Víctor Salmerón <viktormx@protonmail.ch>
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
if [[ $EUID -ne 0 ]]; then
	clear
	echo "este script debe ser ejecutado por el usuario root"  1>&2
	exit 1
else
	clear
	while :
	 do
	 echo " Seleccione el escritorio que desea instalar en su distribución"
	 echo "1. KDE"
	 echo "2. GNOME"
	 echo "3. XFCE"
	 echo "4. LXDE"
   echo "5. Cinnamon"
   echo "6. Mate"
	 echo "7. Salir"
	 echo -n "Seleccione una opción [1 - 7]"
	 read opcion
	 case $opcion in
	 1) echo "¿Desea instalar KDE completo (c), KDE mínimo (m) o salir (s)? (Presione c, m o s)";
      read sn;
        case $sn in
          c) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils kde-full kde-l10n-es;;

          m) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils kde-plasma-desktop kde-l10n-es plasma-widget-networkmanagement kdm;;

					s)clear;
						exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 2) echo "¿Desea instalar GNOME completo (c), GNOME mínimo (m) o salir (s)? (Presione c, m o s)";
      read sn;
        case $sn in
          c) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils gnome;;

          m) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils gnome-core network-manager-gnome;;

					s)clear;
						exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 3) echo "¿Desea instalar XFCE sí/no (este no tiene opción de instalación mínima)? (Presione s/n)";
      read sn;
        case $sn in
          s) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils xfce4 xfce4-goodies lightdm network-manager-gnome;;

					n)clear;
						exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;


	 4) echo "¿Desea instalar LXDE completo (c), LXDE mínimo (m) o salir (s)? (Presione c, m o s)";
      read sn;
        case $sn in
          c) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils lxde;;

          m) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils lxde-core network-manager-gnome;;

					s)clear;
						exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 5) echo "¿Desea instalar Cinnamon completo (c), Cinnamon mínimo (m) o salir (s)? (Presione c, m o s)";
      read sn;
        case $sn in
          c) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils cinnamon-desktop-environment cinnamon-l10n gdm3;;

          m) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils cinnamon-core cinnamon-l10n;;

					s)clear;
						exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 6) echo "¿Desea instalar Mate completo (c), Mate mínimo (m) o salir (s)? (Presione c, m o s)";
      read sn;
        case $sn in
          c) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils mate-desktop-environment-extras network-manager-gnome gdm3;;

          m) echo "se procederá a actualizar los repositorios antes de instalar";
						 sleep 100;
						 aptitude update;
						 aptitude install xorg alsa-base alsa-utils mate-desktop-environment-core network-manager-gnome gdm3;;

					s)clear;
						exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 7) clear;
				 exit 1;;

	 *) echo "$opc es una opción invalida.";
	 echo "Presiona una tecla para continuar...";
	 read foo;;
 	esac
	done
fi
