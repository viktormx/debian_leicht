##  Debian Leicht - Script para la fácil configuración de Debian
##  Copyright 2015  Víctor Salmerón <viktormx@protonmail.ch>
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
if [[ $EUID -ne 0 ]]; then
	clear
	echo "este script debe ser ejecutado por el usuario root"  1>&2
	exit 1
else
	clear
	while :
	 do
	 echo " Escoja el repositorio que le sea más conveniente "
	 echo "1. Repositorio genérico"
	 echo "2. Repositorio Universidad de El Salvador"
	 echo "3. Repositorio MINSAL"
	 echo "4. Salir"
	 echo -n "Seleccione una opción [1 - 4]"
	 read opcion
	 case $opcion in
	 1) echo "Copiando Repositorio";
      cp -v /etc/apt/sources.list /etc/apt/sources.list.backup;
      cp -v data/repositorios/sources.list.generic /etc/apt/sources.list;
      echo "¿Desea actualizar la lista de repositorios en este momento? (s/n)";
      read sn;
        case $sn in
          s) aptitude update;;

						 exit 1;;
          n) clear;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 2) echo "Copiando Repositorio";
      cp -v /etc/apt/sources.list /etc/apt/sources.list.backup;
      cp -v data/repositorios/sources.list.ues /etc/apt/sources.list;
      read sn;
        case $sn in
          s) aptitude update;;

          n) clear;
						 exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 3) echo "Copiando Repositorio";
      cp -v /etc/apt/sources.list /etc/apt/sources.list.backup;
      cp -v data/repositorios/sources.list.minsal /etc/apt/sources.list;
      read sn
        case $sn in
          s) aptitude update;;

          n) clear;
						 exit 1;;

          *) echo "Seleccione 's' para si o 'n' para no";
        esac
      echo "Presione cualquier tecla para continuar";
      read foo;
      clear;;

	 4) clear;
	    exit 1;;

	 *) echo "$opc es una opción invalida.";
	 echo "Presiona una tecla para continuar...";
	 read foo;;
 	esac
	done
fi
